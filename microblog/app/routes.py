from app import app  # From app package import app variable
from flask import render_template


@app.route('/')
@app.route('/index')
def index():
    user = {'username': 'amin'}
    posts = [
        {
            'author': {'username': 'amin'},
            'body': 'Finally i found the time!'
        }, {
            'author': {'username': 'abbas'},
            'body': 'Good for you man... ;)'
        }, {
            'author': {'username': 'ghader'},
            'body': 'What are you talking about?'
        }
    ]
    return render_template('index.html', title='Home', user=user, posts=posts)
